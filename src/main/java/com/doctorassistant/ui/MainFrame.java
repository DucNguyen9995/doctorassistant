package com.doctorassistant.ui;

import com.doctorassistant.db.Patient;
import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.List;

public class MainFrame {
    private JPanel searchPanel;
    private JPanel mainFramePanel;
    private JButton todayBtn;
    private JButton weekBtn;
    private JTextField searchTextField;
    private JRadioButton radioButton1;
    private JRadioButton radioButton2;
    private JRadioButton radioButton5;
    private JRadioButton radioButton6;
    private JRadioButton radioButton9;
    private JRadioButton radioButton10;
    private JPanel recentPatientsPanel;
    private JRadioButton radioButton3;
    private JRadioButton radioButton4;
    private JRadioButton radioButton7;
    private JRadioButton radioButton8;
    private JRadioButton radioButton11;
    private JRadioButton radioButton12;
    private JTable patientTable;
    private JProgressBar searchingProgress;
    private JDateChooser startDateChooser;
    private JDateChooser endDateChooser;

    private List<Patient> patientList;

    public void setData(List<Patient> patientList) {
        this.patientList = patientList;
        DefaultTableModel patientTableModel = new DefaultTableModel();
    }

    public void getData(List<Patient> patientList) {

    }
}
