package com.doctorassistant.supportfiles;

import com.doctorassistant.db.Patient;
import com.doctorassistant.db.PatientJDBCTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("DBLink.xml");

        PatientJDBCTemplate patientJDBCTemplate = (PatientJDBCTemplate)context.getBean("patientJDBCTemplate");

        System.out.println("------------ Create patient ------------");
        Patient a = new Patient();
        a.name = "Best";
        Patient b = new Patient();
        b.name = "Minh";
        Patient c = new Patient();
        c.name = "Binh";
        patientJDBCTemplate.create(a);
        patientJDBCTemplate.create(b);
        patientJDBCTemplate.create(c);

        System.out.println("------------ List patient ------------");
        List<Patient> pList = patientJDBCTemplate.getPatientList();
        for (Patient patient : pList) {
            System.out.println("----***----");
            System.out.println("PK: " + patient.patientPk());
            System.out.println("Name: "+ patient.name);
        }

        System.out.println("------------ Delete patient ------------");
        patientJDBCTemplate.delete(a);

        System.out.println("------------ List patient ------------");
        pList = patientJDBCTemplate.getPatientList();
        for (Patient patient : pList) {
            System.out.println("----***----");
            System.out.println("PK: " + patient.patientPk());
            System.out.println("Name: "+ patient.name);
        }

    }
}
