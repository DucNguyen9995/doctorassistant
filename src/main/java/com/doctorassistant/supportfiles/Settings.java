package com.doctorassistant.supportfiles;

public class Settings {

    private static Settings INSTANCE;

    private Settings() {

    }

    public static Settings sharedInstance() {
        if(INSTANCE == null)
            INSTANCE = new Settings();
        return INSTANCE;
    }

    public String dateFormat() {
        return "yyyy/MM/dd";
    }
}
