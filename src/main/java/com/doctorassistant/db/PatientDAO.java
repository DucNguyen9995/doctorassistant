/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.doctorassistant.db;

import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author ducnguyen
 */
public interface PatientDAO extends HospitalObjDAO<Patient> {
//    CREATE TABLE IF NOT EXISTS `Patients` ( `patient_pk` STRING NOT NULL, `name` varchar(60) NOT NULL, `dob` DATE, `addr` varchar(200), `phone_num` VARCHAR(255), `email` VARCHAR(255), PRIMARY KEY (`patient_pk`));
    List<Patient> getPatientList();
    List<Patient> searchPatient(String patientPk, String name, String phoneNumber, String email);
}
