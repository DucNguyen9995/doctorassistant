/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.doctorassistant.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author ducnguyen
 */
public class PatientMapper implements RowMapper<Patient> {

    public Patient mapRow(ResultSet rs, int i) throws SQLException {
        Patient p = new Patient(rs.getString("patient_pk"));
        p.name = rs.getString("name");
        String date = rs.getString("dob");
        if(date != null && !date.equals(""))
            p.dob = new Date(Long.parseUnsignedLong(rs.getString("dob"))*1000);
        p.addr = rs.getString("addr");
        p.setPhoneNum(rs.getString("phone_num"));
        p.setEmail(rs.getString("email"));
        return p;
    }
    
}
