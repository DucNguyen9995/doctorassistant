package com.doctorassistant.db;

import javax.sql.DataSource;
import java.util.List;

public interface HospitalObjDAO<T extends HospitalObj> {
    void setDataSource(DataSource ds);
    void create(T obj);
    void update(T obj);
    void delete(T obj);
}
