package com.doctorassistant.db;

import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

public abstract class HospitalObjJDBCTemplate {
    protected DataSource dataSource;
    protected JdbcTemplate jdbcTemplateObj;
}
