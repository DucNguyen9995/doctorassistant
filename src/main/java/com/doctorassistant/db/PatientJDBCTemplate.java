/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.doctorassistant.db;

import java.util.List;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author ducnguyen
 */
public class PatientJDBCTemplate extends HospitalObjJDBCTemplate implements PatientDAO {
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObj = new JdbcTemplate(this.dataSource);
        String sql = "CREATE TABLE IF NOT EXISTS `Patients` ( `patient_pk` STRING NOT NULL, `name` varchar(60) NOT NULL, `dob` DATE, `addr` varchar(200), `phone_num` VARCHAR(255), `email` VARCHAR(255), PRIMARY KEY (`patient_pk`));";
        this.jdbcTemplateObj.execute(sql);
    }

    public void create(Patient patient) {
        String sql = "insert into Patients values (?, ?, ?, ?, ?, ?)";
        jdbcTemplateObj.update(sql, patient.patientPk(), patient.name, patient.dob, patient.addr, patient.PhoneNum(), patient.email());
    }

    public void update(Patient patient) {
        String sql = "update Patient set name = ?, dob=?, addr=?, phone_num = ?, email = ? where patient_pk = ?";
        jdbcTemplateObj.update(sql, patient.name, patient.dob, patient.addr, patient.PhoneNum(), patient.email(), patient.patientPk());
    }

    public void delete(Patient patient) {
        String sql = "delete from Patients where patient_pk = ?";
        jdbcTemplateObj.update(sql, patient.patientPk());
    }

    public List<Patient> getPatientList() {
        String sql = "select * from Patients";
        List<Patient> pList = jdbcTemplateObj.query(sql, new PatientMapper());
        return pList;
    }

    public List<Patient> searchPatient(String patientPk, String name, String phoneNumber, String email) {
        String sql = "select * from Patients where patientPk like '%?%' and name like '%?%' and phone_num like '%?%' and email like '%?%'";
        List<Patient> pList = jdbcTemplateObj.query(sql, new PatientMapper(), patientPk, name, phoneNumber, email);
        return pList;
    }
    
}
