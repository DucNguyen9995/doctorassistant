package com.doctorassistant.db;

import java.util.List;

public interface OperationDAO extends HospitalObjDAO<Operation> {
    //    CREATE TABLE IF NOT EXISTS `Patients` ( `patient_pk` STRING NOT NULL, `name` varchar(60) NOT NULL, `dob` DATE, `addr` varchar(200), `phone_num` VARCHAR(255), `email` VARCHAR(255), PRIMARY KEY (`patient_pk`));
    List<Operation> getOperationList();
    List<Operation> searchOperation(String operationPk);
}
