package com.doctorassistant.db;

import java.util.UUID;
import java.util.Vector;

public abstract class HospitalObj {

    protected final String pk;

    protected HospitalObj() {
        pk = UUID.randomUUID().toString();
    }

    protected HospitalObj(String pk) {
        this.pk = pk;
    }

    public abstract Vector<String> prepareData();
}
