/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.doctorassistant.db;

import com.doctorassistant.supportfiles.Settings;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.Vector;

/**
 *
 * @author ducnguyen
 */
public class Patient extends HospitalObj {
    public String name = null;
    public Date dob = null;
    public String addr = null;
    private String phoneNum = null;
    private String email = null;
    
    public Patient() {
        super();
    }
    
    // This method is use to get data from database
    public Patient(String patientPk) {
        super(patientPk);
    }
    
    public String patientPk() {
        return this.pk;
    }
    
    public String PhoneNum() {
        return this.phoneNum;
    }
    
    public boolean setPhoneNum(String phoneNum) {
        // TODO Regex check
        this.phoneNum = phoneNum;
        return true;
    }
    
    public String email() {
        return this.email;
    }
    
    public boolean setEmail(String email) {
        // TODO regex check
        this.email = email;
        return true;
    }

    public Vector<String> prepareData() {
        return null;
    }
}
