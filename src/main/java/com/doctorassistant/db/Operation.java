package com.doctorassistant.db;

import java.util.Date;
import java.util.UUID;
import java.util.Vector;

public class Operation extends HospitalObj {
    private final String patientPk;
    public Date checkInTime;
    public Date checkOutTime;
    public Date operationTime;
    public String operationDesc;

    public Operation(String patientPk) {
        super();
        this.patientPk = patientPk;
    }

    public Operation(String patientPk, String operationPk) {
        super(operationPk);
        this.patientPk = patientPk;
    }

    public Vector<String> prepareData() {
        return null;
    }
}
