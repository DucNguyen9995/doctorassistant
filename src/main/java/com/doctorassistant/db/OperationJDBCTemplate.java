package com.doctorassistant.db;

import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class OperationJDBCTemplate extends HospitalObjJDBCTemplate implements OperationDAO {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObj;


    @Override
    public void setDataSource(DataSource ds) {

    }

    @Override
    public void create(Operation obj) {

    }

    @Override
    public void update(Operation obj) {

    }

    @Override
    public void delete(Operation obj) {

    }

    @Override
    public List<Operation> getOperationList() {
        return null;
    }

    @Override
    public List<Operation> searchOperation(String operationPk) {
        return null;
    }
}
